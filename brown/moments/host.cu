#include <brown/moments/host.h>

void momentsMalloc(moments_t *moments, int threads)
{
    //arrays size
    moments->size_f = threads * sizeof(float);
    moments->size_st = threads * sizeof(curandState_t);

    //device memory allocation
    gpuErrchk(cudaMalloc((void **)&(moments->d_x), moments->size_f));
    gpuErrchk(cudaMalloc((void **)&(moments->d_sv), moments->size_f));
    gpuErrchk(cudaMalloc((void **)&(moments->d_sv2), moments->size_f));
    gpuErrchk(cudaMalloc((void **)&(moments->states), moments->size_st));

    //host memory allocation
    moments->h_x = (float *)malloc(moments->size_f);
    moments->h_sv = (float *)malloc(moments->size_f);
    moments->h_sv2 = (float *)malloc(moments->size_f);
}


/* copy data from device */
void momentsMemcpy(moments_t *moments)
{
    gpuErrchk(cudaMemcpy(moments->h_x, moments->d_x, moments->size_f, cudaMemcpyDeviceToHost));
    gpuErrchk(cudaMemcpy(moments->h_sv, moments->d_sv, moments->size_f, cudaMemcpyDeviceToHost));
    gpuErrchk(cudaMemcpy(moments->h_sv2, moments->d_sv2, moments->size_f, cudaMemcpyDeviceToHost));
}

/* free used memory */
void momentsFree(moments_t *m)
{
    free(m->h_x);
    free(m->h_sv);
    free(m->h_sv2);

    gpuErrchk(cudaFree(m->d_x));
    gpuErrchk(cudaFree(m->d_sv));
    gpuErrchk(cudaFree(m->d_sv2));
    gpuErrchk(cudaFree(m->states));
}