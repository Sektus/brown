#ifndef MOMENTS_H
#define MOMENTS_H

#include <math.h>
#include <brown/moments/host.h>
#include <brown/moments/kernel.h>
#include <brown/rng/rng.h>

void momentsRun(model_t &m, unsigned int block, float beginx, float endx, bool logx);

#endif //MOMENTS_H