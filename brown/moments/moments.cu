#include <brown/moments/moments.h>

static void momentsCalc(model_t &model, moments_t *moments);
static void kernelRun(model_t &model, moments_t &moments, dim3 &blocks, dim3 &threads);

#define N 2

void momentsRun(model_t &m, unsigned int block, float beginx, float endx, bool logx)
{
    //calculate grid shape
    dim3 threads;
    threads.x = block;
    threads.y = 1u;
    threads.z = 1u;
    dim3 blocks;
    blocks.x = (m.paths + threads.x * N - 1) / (threads.x * N);
    blocks.y = 1u;
    blocks.z = 1u;

    moments_t moments;
    constants_t c;

    //allocate memory for all paths
    momentsMalloc(&moments, threads.x * blocks.x * N);

    //initialize rng
    rngInit(moments.states, blocks.x * N, threads.x);

    //system parameters
    float *h_dx = NULL;
    float step = (endx - beginx) / m.points;

    switch (m.domain) {
        case 'a':
            h_dx = &(m.amp);
            break;
        case 'w':
            h_dx = &(m.omega);
            break;
        case 'f':
            h_dx = &(m.force);
            break;
        case 'g':
            h_dx = &(m.gam);
            break;
        case 'd':
            h_dx = &(m.dg);
            break;
        case 'p':
            h_dx = &(m.dp);
            break;
        case 'l':
            h_dx = &(m.lambda);
            break;
    }

    (*h_dx) = beginx;

    //memcpy model to device constant memory
    mbpModelMemcpy(m);

    mbpConstantsInit(m, c);
    mbpConstantsMemcpy(c);

    //print output to stdout
    printf("#%c <<v>> <<v^2>> D_x\n", m.domain);

    for (int i = 0; i < m.points; ++i){
        //run simulation
        kernelRun(m, moments, blocks, threads);

        //get results from device
        momentsMemcpy(&moments);

        //calculate moments and diffusion coef on host
        momentsCalc(m, &moments);

        printf("%e %e %e %e\n", (*h_dx), moments.av, moments.av2, moments.dc);

        (*h_dx) += step;
        mbpModelMemcpyDomain(m);
        
        mbpConstantsInit(m, c);
        mbpConstantsMemcpyDomain(c, m.domain);
    }

    //free memory
    momentsFree(&moments);
}

static void kernelRun(model_t &model, moments_t &moments, dim3 &blocks, dim3 &threads)
{
    if (model.second_order){
        if (model.dg != 0.0f){
            if (model.dp != 0.0f){
                if (model.comp){
                    momentsKernel<true, true, true, true, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                } else {
                    momentsKernel<true, true, true, false, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                }
            } else {
                if (model.comp){
                    momentsKernel<true, true, false, true, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                } else {
                    momentsKernel<true, true, false, false, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                }
            }
        } else {
            if (model.dp != 0.0f){
                if (model.comp){
                    momentsKernel<true, false, true, true, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                } else {
                    momentsKernel<true, false, true, false, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                }
            } else {
                if (model.comp){
                    momentsKernel<true, false, false, true, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                } else {
                    momentsKernel<true, false, false, false, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                }
            }
        }
    } else {
        if (model.dg != 0.0f){
            if (model.dp != 0.0f){
                if (model.comp){
                    momentsKernel<false, true, true, true, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                } else {
                    momentsKernel<false, true, true, false, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                }
            } else {
                if (model.comp){
                    momentsKernel<false, true, false, true, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                } else {
                    momentsKernel<false, true, false, false, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                }
            }
        } else {
            if (model.dp != 0.0f){
                if (model.comp){
                    momentsKernel<false, false, true, true, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                } else {
                    momentsKernel<false, false, true, false, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                }
            } else {
                if (model.comp){
                    momentsKernel<false, false, false, true, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                } else {
                    momentsKernel<false, false, false, false, N><<<blocks, threads>>>(moments.d_x, moments.d_sv, moments.d_sv2, moments.states);
                }
            }
        }
    }
}

/* calculate moments and diffusion coef */
static void momentsCalc(model_t &model, moments_t *moments)
{
    float sv, sv2, sx, sx2;

    int i;
    
    sv = 0.0f;
    sv2 = 0.0f;
    sx = 0.0f;
    sx2 = 0.0f;

    for (i = 0; i < model.paths; ++i){
        sv += moments->h_sv[i];
        sv2 += moments->h_sv2[i];
        sx += moments->h_x[i];
        sx2 += moments->h_x[i] * moments->h_x[i];
    }

    moments->av = sv / (model.periods * model.spp - model.trigger) / model.paths;
    moments->av2 = sv2 / (model.periods * model.spp - model.trigger) / model.paths;

    sx /= model.paths;
    sx2 /= model.paths;
    
    moments->dc = (sx2 - sx * sx) / (4.0f * model.periods * PI / model.omega);
}