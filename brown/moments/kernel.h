#ifndef MOMENTS_KERNEL_H
#define MOMENTS_KERNEL_H

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#include <brown/mbp/mbp.h>

template<bool second_order, bool dg_flag, bool dp_flag, bool comp_flag, int N>
__global__ void momentsKernel(float *d_x, float *d_sv, float *d_sv2, curandState_t *d_states)
{
    int idx = (threadIdx.x + blockIdx.x * blockDim.x) * N;
    
    //cache per thread array elements
    float l_x[N], l_v[N], l_w[N], l_sv[N], l_sv2[N];
    curandState_t l_state[N];

    //folding counters 
    float xfc[N];

    //jump countdown
    int pcd[N];
    
    #pragma unroll N
    for (int i = 0; i < N; ++i){
        l_sv[i] = 0;
        l_sv2[i] = 0;
        l_state[i] = d_states[idx + i];

        //init x, v, w on device
        l_x[i] = curand_uniform(&l_state[i]);
        l_v[i] = curand_uniform(&l_state[i]) * 4.0f - 2.0f;
        l_w[i] = curand_uniform(&l_state[i]) * 2.0f * PI;

        xfc[i] = 0.0f;

        if (second_order){
            pcd[i] = (int) floor( -logf(curand_uniform(&l_state[i]) / mu + 0.5f));
        }
    }

    for (int i = 0; i < spp * periods; ++i){
        #pragma unroll N
        for (int j = 0; j < N; ++j){
            //switch algorithm
            if (second_order){
                mbpPredcorr<dg_flag, dp_flag, comp_flag>(pcd[j], l_state[j], l_x[j], l_v[j], l_w[j]);
            } else {
                mbpEuler<dg_flag, dp_flag, comp_flag>(l_state[j], l_x[j], l_v[j], l_w[j]);
            }   

            //fold path parameters
            mbpFold(l_x[j], &(xfc[j]), 1.0f);
            mbpFold(l_w[j], NULL, 2.0f * PI);

            //calculate moments
            if (i >= trigger)
            {
                l_sv[j] += l_v[j];
                l_sv2[j] += l_v[j] * l_v[j];
            }
        }
    }

    #pragma unroll N
    for (int i = 0; i < N; ++i){
        //write results back to global memory
        d_x[idx + i] = l_x[i] + xfc[i];
        d_sv[idx + i] = l_sv[i];
        d_sv2[idx + i] = l_sv2[i];
        d_states[idx + i] = l_state[i];
    }
}

#endif //MOMENTS_KERNEL_H