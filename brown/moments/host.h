#ifndef MOMENTS_HOST_H
#define MOMENTS_HOST_H

#include <curand_kernel.h>
#include <brown/core/cudaSafe.h>

struct moments {
    float *h_x, *d_x;
    float *h_v, *d_v;
    float *h_w, *d_w;
    float *h_sv, *d_sv;
    float *h_sv2, *d_sv2;

    float av, av2, dc; //moments

    curandState_t *states;

    size_t size_f; //floats size
    size_t size_st; //states size
};

typedef struct moments moments_t;

void momentsMemcpy(moments_t *moments);
void momentsMalloc(moments_t *moments, int threads);
void momentsFree(moments_t *m);

#endif