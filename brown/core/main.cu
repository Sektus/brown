#include <stdio.h>
#include <getopt.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include <brown/moments/moments.h>

typedef enum brownMode {
    MOMENTS = 1
} brownMode_t; 

typedef enum domain{
    ONE_D = 1,
    TWO_D = 2
} domain_t;

static model_t m;
static int block = 64;
static brownMode_t mode = MOMENTS;
static float trans = 0;
static domain_t domain = ONE_D;
static float beginx = 0;
static float endx = 0;
static bool logx = 0;


static struct option options[] = {
    {"amp", required_argument, NULL, 'a'},
    {"omega", required_argument, NULL, 'b'},
    {"force", required_argument, NULL, 'c'},
    {"gam", required_argument, NULL, 'd'},
    {"Dg", required_argument, NULL, 'e'},
    {"Dp", required_argument, NULL, 'f'},
    {"lambda", required_argument, NULL, 'g'},
    {"comp", required_argument, NULL, 'h'},
    {"dev", required_argument, NULL, 'i'},
    {"block", required_argument, NULL, 'j'},
    {"paths", required_argument, NULL, 'k'},
    {"periods", required_argument, NULL, 'l'},
    {"trans", required_argument, NULL, 'm'},
    {"spp", required_argument, NULL, 'n'},
    {"algorithm", required_argument, NULL, 'o'},
    {"mode", required_argument, NULL, 'p'},
    {"domain", required_argument, NULL, 'q'},
    {"domainx", required_argument, NULL, 'r'},
    {"logx", required_argument, NULL, 't'},
    {"points", required_argument, NULL, 'v'},
    {"beginx", required_argument, NULL, 'w'},
    {"endx", required_argument, NULL, 'y'},
    {NULL, 0, NULL, 0}
};

static void parse_cla(int argc, char **argv)
{
    int op;

    //parce command line arguments
    while( (op = getopt_long(argc, argv, "a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:p:q:r:t:v:w:y", options, NULL)) != EOF) {
        switch (op) {
            case 'a':
                m.amp = atof(optarg);
                break;
            case 'b':
                m.omega = atof(optarg);
                break;
            case 'c':
                m.force = atof(optarg);
                break;
            case 'd':
                m.gam = atof(optarg);
                break;
            case 'e':
                m.dg = atof(optarg);
                break;
            case 'f':
                m.dp = atof(optarg);
                break;
            case 'g':
                m.lambda = atof(optarg);
                break;
            case 'h':
                m.comp = atoi(optarg);
                break;
            case 'i':
                gpuErrchk(cudaSetDevice(atoi(optarg)));
                break;
            case 'j':
                block = atoi(optarg);
                break;
            case 'k':
                m.paths = atoi(optarg);
                break;
            case 'l':
                m.periods = atoi(optarg);
                break;
            case 'm':
                trans = atof(optarg);
                break;
            case 'n':
                m.spp = atoi(optarg);
                break;
            case 'o':
                if ( !strcmp(optarg, "predcorr") ){
                    m.second_order = 1;
                }else if ( !strcmp(optarg, "euler") ){
                    m.second_order = 0;
                }
                break;
            case 'p':
                if ( !strcmp(optarg, "moments") ) {
                    mode = MOMENTS;
                }
                break;
            case 'q':
                if ( !strcmp(optarg, "1d")){
                    domain = ONE_D;
                } else {
                    domain = TWO_D;
                }
                break;
            case 'r':
                m.domain = optarg[0];
                break;
            case 't':
                logx = atoi(optarg);
                break;
            case 'v':
                m.points = atoi(optarg);
                break;
            case 'w':
                beginx = atof(optarg);
                break;
            case 'y':
                endx = atof(optarg);
                break;
        }
    }
    m.trigger = trans * m.periods * m.spp;
    m.paths = ((m.paths + block - 1) / block) * block;
}


int main(int argc, char **argv)
{
    parse_cla(argc, argv);

    if (mode == MOMENTS && domain == ONE_D){
        momentsRun(m, block, beginx, endx, logx);
    }

    return 0;
}
