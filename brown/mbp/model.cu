#include <brown/mbp/model.h>

/* Device constants space */
__constant__ float amp, omega, force, gam;
__constant__ unsigned int periods, spp, trigger;
__constant__ float two_pi, diff, comp, ampmean, mu, half_step, wdt, dt;

/* Memcpy model from host to device */
void mbpModelMemcpy(model_t &m)
{
    gpuErrchk(cudaMemcpyToSymbol(amp, (void*) &(m.amp), sizeof(float), 0, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpyToSymbol(omega, (void*) &(m.omega), sizeof(float), 0, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpyToSymbol(force, (void*) &(m.force), sizeof(float), 0, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpyToSymbol(gam, (void*) &(m.gam), sizeof(float), 0, cudaMemcpyHostToDevice));

    gpuErrchk(cudaMemcpyToSymbol(periods, (void*) &(m.periods), sizeof(unsigned int), 0, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpyToSymbol(spp, (void*) &(m.spp), sizeof(unsigned int), 0, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpyToSymbol(trigger, (void*) &(m.trigger), sizeof(unsigned int), 0, cudaMemcpyHostToDevice));
}

void mbpConstantsMemcpy(constants_t &c)
{
    gpuErrchk(cudaMemcpyToSymbol(diff, (void*) &(c.diff), sizeof(float), 0, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpyToSymbol(comp, (void*) &(c.comp), sizeof(float), 0, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpyToSymbol(ampmean, (void*) &(c.ampmean), sizeof(float), 0, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpyToSymbol(mu, (void*) &(c.mu), sizeof(float), 0, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpyToSymbol(half_step, (void*) &(c.half_step), sizeof(float), 0, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpyToSymbol(wdt, (void*) &(c.wdt), sizeof(float), 0, cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpyToSymbol(dt, (void*) &(c.dt), sizeof(float), 0, cudaMemcpyHostToDevice));
}

void mbpConstantsMemcpyDomain(constants_t &c, char domain)
{
    switch (domain){
        case 'w':
            mbpConstantsMemcpy(c);
            break;
        case 'g':
            gpuErrchk(cudaMemcpyToSymbol(diff, (void*) &(c.diff), sizeof(float), 0, cudaMemcpyHostToDevice));
            break;
        case 'd':
            gpuErrchk(cudaMemcpyToSymbol(diff, (void*) &(c.diff), sizeof(float), 0, cudaMemcpyHostToDevice));
            break;
        case 'p':
            gpuErrchk(cudaMemcpyToSymbol(comp, (void*) &(c.comp), sizeof(float), 0, cudaMemcpyHostToDevice));
            gpuErrchk(cudaMemcpyToSymbol(ampmean, (void*) &(c.ampmean), sizeof(float), 0, cudaMemcpyHostToDevice));
            break;
        case 'l':
            gpuErrchk(cudaMemcpyToSymbol(comp, (void*) &(c.comp), sizeof(float), 0, cudaMemcpyHostToDevice));
            gpuErrchk(cudaMemcpyToSymbol(ampmean, (void*) &(c.ampmean), sizeof(float), 0, cudaMemcpyHostToDevice));
            gpuErrchk(cudaMemcpyToSymbol(mu, (void*) &(c.mu), sizeof(float), 0, cudaMemcpyHostToDevice));
            break;
    }
}

void mbpModelMemcpyDomain(model_t &m)
{
    switch (m.domain) {
        case 'a':
            gpuErrchk(cudaMemcpyToSymbol(amp, (void*) &(m.amp), sizeof(float), 0, cudaMemcpyHostToDevice));
            break;
        case 'w':
            gpuErrchk(cudaMemcpyToSymbol(omega, (void*) &(m.omega), sizeof(float), 0, cudaMemcpyHostToDevice));
            break;
        case 'f':
            gpuErrchk(cudaMemcpyToSymbol(force, (void*) &(m.force), sizeof(float), 0, cudaMemcpyHostToDevice));
            break;
        case 'g':
            gpuErrchk(cudaMemcpyToSymbol(gam, (void*) &(m.gam), sizeof(float), 0, cudaMemcpyHostToDevice));
            break;
    }
}