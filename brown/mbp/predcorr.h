#ifndef MBP_PREDCORR_H
#define MBP_PREDCORR_H

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#include <brown/mbp/model.h>

template<bool dp_flag, bool comp_flag>
__inline__ __device__ static float adaptedJump(int &pcd, curandState_t &state)
{
    if (dp_flag) {
        if (pcd <= 0) {
            pcd = (int) floor(-logf(curand_uniform(&state)) / mu + 0.5f);
            
            if (comp_flag) {
                return -logf(curand_uniform(&state)) / ampmean - comp;
            } else {
                return -logf(curand_uniform(&state)) / ampmean;
            }
        } else {
            --pcd;
            if (comp_flag) {
                return -comp;
            } else {
                return 0.0f;
            }
        }
    } else {
        return 0.0f;
    }
}

template<bool dg_flag>
__inline__ __device__ static float secondOrderDiffusion(curandState_t &state)
{
    if (dg_flag) {
        const float r = curand_uniform(&state);
        if ( r <= 1.0f / 6.0f ) {
            return -diff;
        } else if ( r > 1.0f / 6.0f && r <= 2.0f / 6.0f ) {
            return diff;
        } else {
            return 0.0f;
        }
    } else {
        return 0.0f;
    }
}

/* Simplified weak order 2.0 adapted predictor-corrector scheme
 * ( see E. Platen, N. Bruti-Liberati; Numerical Solution of Stochastic Differential
 * Equations with Jumps in Finance; Springer 2010; p. 503, p. 532 )
 */
template<bool dg_flag, bool dp_flag, bool comp_flag>
__inline__ __device__ static void mbpPredcorr(int &pcd, curandState_t &state, float &x, float &v,
    float &w)
{
    float l_xt, l_xtt, predl_x;
    float l_vt, l_vtt, predl_v;
    
    l_xt = v;
    l_vt = mbpDrift(x, v, w);
    w += wdt;
    
    predl_x = x + l_xt * dt;
    predl_v = v + l_vt * dt + secondOrderDiffusion<dg_flag>(state);
    
    l_xtt = predl_v;
    l_vtt = mbpDrift(predl_x, predl_v, w);

    predl_x = x + half_step * (l_xt + l_xtt);
    predl_v = v + half_step * (l_vt + l_vtt) +
              secondOrderDiffusion<dg_flag>(state);

    l_xtt = predl_v;
    l_vtt = mbpDrift(predl_x, predl_v, w);

    x += half_step * (l_xt + l_xtt);
    v += half_step * (l_vt + l_vtt) +
            secondOrderDiffusion<dg_flag>(state) +
            adaptedJump<dp_flag, comp_flag>(pcd, state);
}

#endif //MBP_PREDCORR_H