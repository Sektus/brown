#ifndef MBP_EULER_H
#define MBP_EULER_H

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#include <brown/mbp/model.h>

template<bool dp_flag, bool comp_flag>
__inline__ __device__ static float regularJump(curandState_t &state)
{
    if (dp_flag) {
        const unsigned int n = curand_poisson(&state, mu);
        float s = 0.0f;
        int i;

        for (i = 0; i < n; ++i) {
            s += -logf(curand_uniform(&state)) / ampmean;
        }

        if (comp_flag){
            s -= comp;
        }

        return s;
    } else {
        return 0.0f;
    }
}

template<bool dg_flag>
__inline__ __device__ static float firstOrderDiffusion(curandState_t &state)
{
    if (dg_flag) {
        const float r = curand_uniform(&state);
        if ( r <= 0.5f ) {
            return -diff;
        } else {
            return diff;
        }
    } else {
        return 0.0f;
    }
}

/* Simplified weak order 1.0 regular euler-maruyama scheme
 * ( see E. Platen, N. Bruti-Liberati; Numerical Solution of Stochastic Differential
 * Equations with Jumps in Finance; Springer 2010; p. 508,
 *  Kim, E. Lee, P. Talkner, and P.Hanggi; Phys. Rev. E 76; 011109; 2007 )
 */
template<bool dg_flag, bool dp_flag, bool comp_flag>
__inline__ __device__ static void mbpEuler(curandState_t &state, float &x, float &v, float &w)
{
    float l_xt;
    float l_vt;

    l_xt = x + v * dt;
    l_vt = v + mbpDrift(x, v, w) * dt +
            firstOrderDiffusion<dg_flag>(state) +
            regularJump<dp_flag, comp_flag>(state);
    w += wdt;

    x = l_xt;
    v = l_vt;
}

#endif //MBP_EULER_H