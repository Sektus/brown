#ifndef MBP_MODEL_H
#define MBP_MODEL_H

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#include <brown/core/cudaSafe.h>
#include <stdbool.h>

#define PI 3.14159265358979f

typedef struct model {
    bool second_order;
    bool comp;
    float amp;
    float omega;
    float force;
    float gam;
    float dg;
    float dp;
    float lambda;
    
    //simulation parameters
    unsigned int paths;
    unsigned int points;
    unsigned int periods;
    unsigned int spp;
    unsigned int trigger;
    char domain;
} model_t;

typedef struct constants
{
    float diff;
    float comp;
    float ampmean;
    float mu;
    float half_step;
    float wdt;
    float dt;
} constants_t;

extern __constant__ float amp, omega, force, gam;
extern __constant__ unsigned int periods, spp, trigger;
extern __constant__ float diff, comp, ampmean, mu, half_step, wdt, dt;
extern __constant__ bool comp_flag;

void mbpModelMemcpy(model_t &m);
void mbpModelMemcpyDomain(model_t &m);
void mbpConstantsMemcpy(constants_t &c);
void mbpConstantsMemcpyDomain(constants_t &c, char domain);

/* Reduce periodic variable to the base domain */
__inline__ __device__ static void mbpFold(float  &nx, float  *xfc, const float &c)
{
    if (fabs(nx) > c){
        float temp = floor(nx / c) * c;

        nx -= temp;

        if (xfc != NULL){
            (*xfc) += temp;
        }
    }
}

/* Particle drift */
__inline__ __device__ static float mbpDrift(const float &x, const float &v, const float &w)
{
    return -(gam * v) - 2.0f * PI * cosf(2.0f * PI * x) + amp * cosf(w) + force;
}

/* device constant struct initialization */
__inline__ __host__ static void mbpConstantsInit(const model_t &m, constants_t &c)
{
    c.dt = 2.0f * PI / m.omega / m.spp;

    if (m.second_order)
    {
        c.diff = sqrtf(6.0f * m.gam * m.dg * c.dt);
    } else {
        c.diff = sqrtf(2.0f * m.gam * m.dg * c.dt);
    }
    
    c.comp = sqrtf(m.dp * m.lambda) * c.dt;
    c.ampmean = sqrtf(m.lambda / m.dp);
    c.mu =  m.lambda * c.dt;
    c.half_step = c.dt * 0.5f;
    c.wdt = c.dt * m.omega;
}

#endif //MBP_MODEL_H