#ifndef RNG_KERNEL_H
#define RNG_KERNEL_H

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

__global__ void rngInitKernel(unsigned int *seeds, curandState_t *states);


#endif //MOMENTS_KERNEL_H