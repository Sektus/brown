#include <brown/rng/kernel.h>

__global__ void rngInitKernel(unsigned int *seeds, curandState_t *states)
{
    int idx = threadIdx.x + blockIdx.x * blockDim.x;

    //cache in local variables
    unsigned int seed = seeds[idx];
    curandState_t state = states[idx];

    #ifndef TEST
    curand_init(seed, idx, 0, &state);
    #else 
    curand_init(idx, idx, 0, &states[idx]);
    #endif

    states[idx] = state;
}