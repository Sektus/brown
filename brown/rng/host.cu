#include <brown/rng/host.h>


void rngInit(curandState_t *states, int b, int t)
{
    curandGenerator_t gen;
    unsigned int *h_seeds, *d_seeds;
    size_t size_ui; //seeds size

    unsigned int paths = b * t; 

    size_ui = paths * sizeof(unsigned int);

    gpuErrchk(cudaMalloc((void **)&d_seeds, size_ui));
    h_seeds = (unsigned int *)malloc(size_ui);

    curandCreateGeneratorHost(&gen, CURAND_RNG_PSEUDO_DEFAULT);
    curandSetPseudoRandomGeneratorSeed(gen, time(NULL));

    curandGenerate(gen, h_seeds, paths);

    gpuErrchk(cudaMemcpy(d_seeds, h_seeds, size_ui, cudaMemcpyHostToDevice));

    rngInitKernel<<<b, t>>>(d_seeds, states);

    free(h_seeds);
    gpuErrchk(cudaFree(d_seeds));
    curandDestroyGenerator(gen);
}