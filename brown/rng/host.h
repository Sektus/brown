#ifndef RNG_HOST_H
#define RNG_HOST_H

#include <cuda.h>
#include <curand.h>
#include <time.h>

#include <brown/core/cudaSafe.h>
#include <brown/rng/kernel.h>

void rngInit(curandState_t *states, int b, int t);

#endif //RNG_HOST_H