TARGET_LIB = $(LIBDIR)/libbrown.so
TARGET_APP = prog
TARGET_PY = python

#.SUFFIXES += .cu

OBJDIR = ./.obj
SRCDIR = ./brown
LIBDIR = ./lib
#dependencies
DEPDIR = $(OBJDIR)/.deps
DEPFLAGS = $(INCLUDES) -MM -MT $(OBJDIR)/$*.o -MF $(DEPDIR)/$*.d

#nvcc flags
NVCC = nvcc
NVCCFLAGS = -arch=compute_50 -code=sm_50 -m64 --use_fast_math -O3 -Xcompiler -fPIC
LDFLAGS = -L/usr/local/cuda/lib64 -lcurand -lm
INCLUDES = -I./include -I. -I/usr/local/cuda/include/
COMPILE.cu = $(NVCC) -x cu -Xptxas -v $(NVCCFLAGS) $(INCLUDES)  -dc
DEPS.cu = $(NVCC) $(DEPFLAGS)

SRCS = $(shell find ./brown -type f -name "*.cu")
HEADS = $(shell find ./brown -type f -name "*.h")
OBJS = $(SRCS:$(SRCDIR)/%.cu=$(OBJDIR)/%.o)
DEPFILES = $(SRCS:$(SRCDIR)/%.cu=$(DEPDIR)/%.d)

.PHONY: clean all $(TARGET_PY)
.SECONDARY: $(DEPFILES)

all: $(TARGET_LIB) $(TARGET_APP)

$(TARGET_LIB): $(OBJS) $(HEADS) | $(LIBDIR)
	$(NVCC) $(NVCCFLAGS) -o $@ --shared $(OBJS) $(LDFLAGS)
	#sudo cp $@ /lib

$(OBJDIR)/%.o: $(SRCDIR)/%.cu $(DEPDIR)/%.d | $(OBJDIR)
	@mkdir -p $(@D)
	$(COMPILE.cu) $(OUTPUT_OPTION) $< $(LDFLAGS)

$(DEPDIR)/%.d: $(SRCDIR)/%.cu | $(DEPDIR)
	@mkdir -p $(@D)
	$(DEPS.cu) $<

$(TARGET_PY): | $(TARGET_LIB)
	python3 setup.py install --user

$(DEPDIR): ;	@mkdir -p $@
$(LIBDIR): ;	@mkdir -p $@
$(OBJDIR): ;	@mkdir -p $@

include $(wildcard $(DEPFILES))

$(TARGET_APP): $(OBJS)
	$(NVCC) $(NVCCFLAGS) $(OBJS) -o $@ $(LDFLAGS)

clean:
	$(RM) $(TARGET_LIB) $(TARGET_APP) $(OBJS) $(DEPFILES)
	#$(TARGET_LIB) $(TARGET_APP) $(OBJS) $(DEPFILES)
	#rm -rf build
